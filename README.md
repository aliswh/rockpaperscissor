First project in Python3.

Simple Rock, Paper, Scissors game.

To give the result without storing each result by comparing strings, I gave to each move a number, from 1 to 3.
[Rock - 1, Paper - 2, Scissors - 3]
The user move is divided by the (random) computer move. This means that there are only 6 possible results.
If the result is between 1/2 and 3/2, the user will loose. Outside this range, the user will win. 

[possible improvements]
- UI
- data analysis
- data rappresentation