from random import randrange
import math

rock = 1    #   wins to scissor 1/3, loses to paper 1/2
paper = 2   #   wins to rock 2/1, loses to scissor 2/3
scissor = 3 #   wins to paper 3/1, loses to rock 3/2

gameRunning = True
keepPlaying=  True
wincount = 0
losscount = 0
tiedcount = 0

#keep playing till you type yes
while gameRunning==True :
    mymove = input('Rock [r], paper [p] or scissor [s]? : ')
    cpumove = randrange(1,3)
    
    switcher_letterToNumber = {
            'r': 1,
            'p': 2,
            's': 3
        }

    switcher_numberToLetter = {
        y:x for x,y in switcher_letterToNumber.items()
    }

    def getmovenumber(mymove):
        fun = switcher_letterToNumber.get(mymove, 'invalid')
        return fun

    def getmovename(cpumove):
        fun = switcher_numberToLetter.get(cpumove)
        return fun

    mymoveint = getmovenumber(mymove)

    print ('Computer got ', getmovename(cpumove))

    # if I get a result from 1/2 to 3/2, I lose
    if mymoveint == cpumove:
        tiedcount += 1
        print('TIED!')
    elif (1/2 <= mymoveint/cpumove <= 3/2):
        losscount += 1
        print('LOSS!')
    else: 
        wincount += 1
        print('WIN!')
    
    #ask if you want to exit the game, don't exit till you get yes or no
    while keepPlaying==True :
        inputexit = input('Do you want to exit? (type yes or no) : ')
        if inputexit=='yes':
            print ('You won ', wincount, ' matches, you tied ', tiedcount, ', you lost ', losscount)
            exit()
        elif inputexit=='no':
            break


